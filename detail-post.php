<html>
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>Hoạt động thiện nguyện sinh viên</title>
    <link rel="stylesheet" href="vendor/bootstrap-4.1.1-dist/css/bootstrap.min.css">
    <link rel="stylesheet" href="vendor\fontawesome-free-5\css\all.min.css">
    <link rel="stylesheet" href="css/style.css">
</head>
<body>
    <!-- Top header -->
    <div id="top-header" class="top-header bg-danger">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="d-flex align-items-center">
                        <div class="p-2 mr-auto">
                            <img class="img-fluid" src="img/logo.png" alt="logo website">
                        </div>
                        <div class="d-flex p-2 align-items-center">
                            <div class="login mr-md-2 mr-lg-5">
                                <a class="mr-3" href="#">Đăng nhập</a> <a href="#">Đăng ký</a>
                            </div>
                            <div class="search-form">
                                <form action="#" method="post">
                                <input type="search" name="search" class="form-control" placeholder="Search">
                                <button type="submit">
                                    <i class="fa fa-search" aria-hidden="true"></i>
                                </button>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Menu -->
    <div class="menu bg-light">
        <div class="container">
            <nav class="navbar navbar-expand-md navbar-light">			  
				<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
					<span class="navbar-toggler-icon"></span>
				</button>
				<div class="collapse navbar-collapse" id="navbarNavDropdown">
					<ul class="navbar-nav w-100 justify-content-around">
						<li class="nav-item active">
							<a class="nav-link" href="#">Trang chủ <span class="sr-only">(current)</span></a>
						</li>
						<li class="nav-item">
							<a class="nav-link" href="#">Giới thiệu</a>
						</li>			     
						<li class="nav-item dropdown">
							<a class="nav-link dropdown-toggle" href="#" id="navbarDropdownHoatDong" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
								Hoạt động
							</a>
							<div class="dropdown-menu" aria-labelledby="navbarDropdownHoatDong">
								<a class="dropdown-item" href="#">Chiến dịch thiện nguyện hè</a>
								<a class="dropdown-item" href="#">Công tác giáo dục</a>			          
							</div>
						</li>
						<li class="nav-item">
							<a class="nav-link" href="#">Liên hệ</a>
						</li>
					</ul>
				</div>
			</nav>
        </div>
    </div>
    <div class="container mt-3">
        <!-- Điều hướng -->
        <div class="row">
            <div class="col-12">
                <ul class="breadcrumb">
                    <li><a href="#">Hoạt động</a></li>
                    <li><a href="#">Chiến dịch tình nguyện hè</a></li>
                </ul>
            </div>
        </div>
        <!-- Bài viết chi tiết và bài viết liên quan -->
        <div class="row mt-3">
            <!--Bài viết chi tiết-->
            <div class="col-lg-9 single-post">
                <img src="img/tin1.jpg" alt="Để trở thành sinh viên năng động cần như thế nào?" class="post-thumb w-100">
                <h1 class="title">
                    Để trở thành sinh viên năng động cần như thế nào?</h1>
                <div>
                    <span class="author"><i class="fas fa-user"></i>Sưu tầm</span> <span class="time"><i
                        class="far fa-calendar-check"></i>27 Tháng Bảy, 2018</span>
                </div>
                <!-- Nội dung bài viết -->
                <div class="content mt-4">
                    <div class="entry-content">
                        <p style="text-align: justify">
                            <strong>Bạn cần nắm bắt đúng thời cơ để trở thành những sinh viên năng động. Một người
                                trẻ năng động sẽ có nhiều cơ hội để học hỏi thêm những điều mới mẻ và có ích cho
                                bản thân. </strong>
                        </p>
                        <p style="text-align: justify">
                            Hiện nay tại các trường Đại học – Cao đẳng đều có các hoạt động ngoại khóa vì thế
                            sinh viên nên tận dụng cơ hội này để có thể chọn cho mình một hoạt động như: thể
                            thao, câu lạc bộ học thuật…. Tùy theo thời gian của bạn như thế nào? Bạn dành được
                            bao nhiêu thời gian vào những hoạt động đó. Các hoạt động ngoại khóa sẽ giúp bạn
                            xây dựng được tính cách thông qua khi làm việc tập thể, hoạt động nhóm. Quản lý
                            thời gian và cạnh tranh từ những hoạt động đó bạn sẽ không phải lo lắng quá nhiều
                            nếu bạn không giỏi về những gì mà bạn thích. Không nên tự ti điều quan trọng là
                            phải có niềm đam mê, chình từ những điều đó sẽ thúc đẩy bạn nỗ lực hết mình.</p>
                        <figure id="attachment_146" style="width: 600px" class="wp-caption aligncenter"><img class="wp-image-146 size-full" src="http://thirdtext.com/wp-content/uploads/2018/07/de-tro-thanh-sinh-vien-nang-dong-can-nhu-the-nao.jpg" alt="" width="600" height="397" srcset="http://thirdtext.com/wp-content/uploads/2018/07/de-tro-thanh-sinh-vien-nang-dong-can-nhu-the-nao.jpg 600w, http://thirdtext.com/wp-content/uploads/2018/07/de-tro-thanh-sinh-vien-nang-dong-can-nhu-the-nao-300x199.jpg 300w" sizes="(max-width: 600px) 100vw, 600px"><figcaption class="wp-caption-text"><em>Để trở thành sinh viên năng động cần như thế nào?</em></figcaption></figure>
                        <p style="text-align: justify">
                            Hoạt động tình nguyện: Nếu bạn có nhiều thời gian hãy dành những khoảng không đó
                            vào các hoạt động tình nguyện, dành thời gian cho cộng động với những hoạt động
                            cao quý đem đến nhiều lợi ích cho bạn. Tình nguyên cũng có thể dạy bạn các kỹ năng
                            liên quan đến công việc ví dụ như: Trách nhiệm hoặc việc quản lý thời gian … Tại
                            nhiều công ty các hoạt động tình nguyện sẽ giúp cho bạn có môi trường làm việc hữu
                            ích đồng thời cũng hấp dẫn hơn đối với nhiều bạn trẻ. Thời gian nghỉ hè bạn cũng
                            có thể tham gia các chương trình học tập cũng như các hoạt động trại hè để có thêm
                            kinh nghiệm. Hiện nay các trường đại học cũng đã cung cấp các chương trình cho sinh
                            viên đại học dựa trên các mối quan tâm như: báo chí, nhiếp ảnh, nghệ thuật và thể
                            thao…</p>
                        <p style="text-align: justify">
                            Các chương trình bạn nên tìm ra cái nào là sự lựa chọn tốt nhất đồng thời phân tích
                            sự khác nhau giữa các hoạt động. Tất cả những hành động này bạn cần kiểm tra các
                            yêu cầu cũng như thời hạn để tránh gặp những rắc rối vì những sai lầm tưởng như
                            là đơn gian. Thời gian tham gia các hoạt động cộng đồng sẽ giúp bạn có một khoảng
                            thời gian bổ ích và tuyệt vời.</p>
                        <p style="text-align: justify">
                            Một lĩnh vực bạn theo đuổi cũng là cơ hội để bạn đánh bóng tên tuổi cũng như xây
                            dựng được các sơ yếu lý lịch, những kinh nghiệm khi còn rất trẻ. Để sau này khi
                            vào các công ty hoặc nơi bạn làm việc sẽ tự tin hơn. Cách trưởng thành này cũng
                            là con đường sự nghiệp các bạn trẻ hiện nay cần và nên áp dụng nó.</p>
                        <figure id="attachment_145" style="width: 640px" class="wp-caption aligncenter"><img class="wp-image-145 size-full" src="http://thirdtext.com/wp-content/uploads/2018/07/de-tro-thanh-sinh-vien-nang-dong-can-nhu-the-nao-1.jpg" alt="" width="640" height="427" srcset="http://thirdtext.com/wp-content/uploads/2018/07/de-tro-thanh-sinh-vien-nang-dong-can-nhu-the-nao-1.jpg 640w, http://thirdtext.com/wp-content/uploads/2018/07/de-tro-thanh-sinh-vien-nang-dong-can-nhu-the-nao-1-300x200.jpg 300w, http://thirdtext.com/wp-content/uploads/2018/07/de-tro-thanh-sinh-vien-nang-dong-can-nhu-the-nao-1-570x380.jpg 570w" sizes="(max-width: 640px) 100vw, 640px"><figcaption class="wp-caption-text"><em>Hoạt động tình nguyện</em></figcaption></figure>
                        <p style="text-align: justify">
                            Ngoài những hoạt động trải nghiệm thì việc học tiếng anh cũng rất cần thiết đối
                            với các sinh viên. Khi học sẽ là một khoảng thời gian tốt để bạn có thêm nhiều kinh
                            nghiệm cũng như có cơ hội để tận dụng những kiến thức hay, hấp dẫn và mở mang đầu
                            óc. Ngôn ngữ là phương tiện để giao tiếp giữa người với người trở nên gần nhau và
                            hiểu nhau hơn. Chính vì thế khả năng giao tiếp đóng góp vai trò cực kỳ quan trọng
                            để các bạn có thể thấy thoải mái thể hiện được các thế mạnh của bản thân.</p>
                        <p style="text-align: justify">
                            Trong khoảng thời gian làm sinh viên bạn cần phải tập trung vào việc học để đạt
                            được kết quả cao. Việc làm này không những giúp bạn không bỏ lỡ những kiến thức
                            cơ bản, nếu bạn không chú ý học thì sẽ để lại những hậu quả như : học lại, thi lại,
                            điểm thấp… Với việc tham gia các hoạt động ngoại khóa song song với việc học cũng
                            phải đặc biệt chú ý tới. Tránh việc mất thời gian và mất thêm về điều kiện kinh
                            tế. Trên đây là những chia sẻ của <a href="http://thirdtext.com"><strong>thirdtext</strong></a>
                            hy vọng sẽ giúp bạn có được những trải nghiệm mới từ môi trường đại học.</p>
                    </div>
                </div>
            </div>
            <!--Bài viết liên quan-->
            <div class="col-lg-3">
                <div class="row mb-3">
                    <div class="col-12">
                        <div class="title-section">
                            <h4 class="bg-danger p-3 text-white w-100">
                                Bài viết liên quan</h4>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12 mb-2">
                        <div class="card w-100">
                            <div class="row">
                                <div class="col-5">
                                    <img class="card-img-top" src="img/tin1.jpg" alt="Để trở thành sinh viên năng động cần như thế nào?">
                                </div>
                                <div class="col">
                                    <div class="card-body p-0">
                                        <h6 class="card-title line-clamp-2">
                                            Để trở thành sinh viên năng động cần như thế nào?</h6>
                                        <p class="card-time">
                                            10/10/2019 10:10</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12 mb-2">
                        <div class="card w-100">
                            <div class="row">
                                <div class="col-5">
                                    <img class="card-img-top" src="img/tin1.jpg" alt="Để trở thành sinh viên năng động cần như thế nào?">
                                </div>
                                <div class="col">
                                    <div class="card-body p-0">
                                        <h6 class="card-title line-clamp-2">
                                            Để trở thành sinh viên năng động cần như thế nào?</h6>
                                        <p class="card-time">
                                            10/10/2019 10:10</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12 mb-2">
                        <div class="card w-100">
                            <div class="row">
                                <div class="col-5">
                                    <img class="card-img-top" src="img/tin1.jpg" alt="Để trở thành sinh viên năng động cần như thế nào?">
                                </div>
                                <div class="col">
                                    <div class="card-body p-0">
                                        <h6 class="card-title line-clamp-2">
                                            Để trở thành sinh viên năng động cần như thế nào?</h6>
                                        <p class="card-time">
                                            10/10/2019 10:10</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12 mb-2">
                        <div class="card w-100">
                            <div class="row">
                                <div class="col-5">
                                    <img class="card-img-top" src="img/tin1.jpg" alt="Để trở thành sinh viên năng động cần như thế nào?">
                                </div>
                                <div class="col">
                                    <div class="card-body p-0">
                                        <h6 class="card-title line-clamp-2">
                                            Để trở thành sinh viên năng động cần như thế nào?</h6>
                                        <p class="card-time">
                                            10/10/2019 10:10</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12 mb-2">
                        <div class="card w-100">
                            <div class="row">
                                <div class="col-5">
                                    <img class="card-img-top" src="img/tin1.jpg" alt="Để trở thành sinh viên năng động cần như thế nào?">
                                </div>
                                <div class="col">
                                    <div class="card-body p-0">
                                        <h6 class="card-title line-clamp-2">
                                            Để trở thành sinh viên năng động cần như thế nào?</h6>
                                        <p class="card-time">
                                            10/10/2019 10:10</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12 mb-2">
                        <button type="button" class="btn btn-info w-100">
                            Xem thêm</button>
                    </div>
                </div>
            </div>
        </div>
        <!-- Bình luận -->
        <div class="row mt-4 mb-4 comment">
            <div class="col-12 title pl-0">
                Bình luận</div>
            <div class="col-12 write-comment pl-0 pr-0">
                <div class="input-group mb-3">
					<textarea class="form-control" placeholder="Người đăng" rows="3"></textarea>
                    <textarea class="form-control" placeholder="Nội dung bình luận" rows="3"></textarea>
                    <div class="input-group-append">
                        <button class="btn btn-success" type="submit">
                            Gửi bình luận</button>
                    </div>
                </div>
            </div>
            <div class="col-12 media border p-3">
                <img src="img/img_avatar3.png" alt="John Doe" class="mr-3 mt-3 rounded-circle" style="width: 60px;">
                <div class="media-body">
                    <h4>
                        Nguyễn An <small><i>Đăng ngày 19 Tháng Một, 2016</i></small></h4>
                    <p>
                        Bài viết rất hữu ích</p>
                    <div class="media p-3">
                        <img src="img/img_avatar2.png" alt="Jane Doe" class="mr-3 mt-3 rounded-circle" style="width: 45px;">
                        <div class="media-body">
                            <h4>
                                Phạm Thanh Hóa <small><i>Đăng ngày 23 Tháng Hai, 2018</i></small></h4>
                            <p>
                                Đã qua một thời sinh viên, đã từng có những trải nghiệm như bài viết</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>    
    <!-- Footer -->
    <div class="footer mt-3 text-light">
        <div class="container-fluid">
            <div class="container">
                <div class="row pt-3 pb-3">
                    <!-- Logo + liên hệ -->
                    <div class="col-lg-3">
                        <div class="p-2 mr-auto">
                            <img class="img-fluid" src="img/logo.png" alt="logo website">
                            <div class="mt-3">
                                <strong>Liên hệ:</strong>
                                <br>
                                <i class="fas fa-map-marker-alt text-danger"></i>65 Huỳnh Thúc Kháng, P.Bến Nghé,
                                Q.1, TP.HCM
                            </div>
                        </div>
                    </div>
                    <!-- Danh mục 1 -->
                    <div class="col-lg col-4">
                        <h5>
                            Danh mục 1</h5>
                        <ul>
                            <li>Danh mục 1.1</li>
                            <li>Danh mục 1.2</li>
                            <li>Danh mục 1.3</li>
                            <li>Danh mục 1.4</li>
                            <li>Danh mục 1.5</li>
                        </ul>
                    </div>
                    <!-- Danh mục 2 -->
                    <div class="col-lg col-4">
                        <h5>
                            Danh mục 2</h5>
                        <ul>
                            <li>Danh mục 2.1</li>
                            <li>Danh mục 2.2</li>
                            <li>Danh mục 2.3</li>
                            <li>Danh mục 2.4</li>
                            <li>Danh mục 2.5</li>
                        </ul>
                    </div>
                    <!-- Danh mục 3 -->
                    <div class="col-lg col-4">
                        <h5>
                            Danh mục 3</h5>
                        <ul>
                            <li>Danh mục 3.1</li>
                            <li>Danh mục 3.2</li>
                            <li>Danh mục 3.3</li>
                            <li>Danh mục 3.4</li>
                            <li>Danh mục 3.5</li>
                        </ul>
                    </div>
                    <!-- Đóng góp -->
                    <div class="col-lg-3 text-right">
                        <textarea class="form-control" rows="4" id="comment" placeholder="Mời bạn đóng góp ý kiến"></textarea>
                        <button type="button" class="btn btn-warning mt-2 w-100">
                            Gửi</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Nút quay về đầu trang -->
    <div class="btn-go-up">
        <a href="#top-header" class="fas fa-arrow-circle-up"></a>
    </div>
    <script src="vendor/js/jquery-3.3.1.min.js"></script>
    <script src="vendor/js/popper.min.js"></script>
    <script src="vendor/bootstrap-4.1.1-dist/js/bootstrap.min.js"></script>
    <script src="js/script.js"></script>
</body>
</body>
</html>
