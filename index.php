<html>
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<title>Hoạt động thiện nguyện sinh viên</title>
	<link rel="stylesheet" href="vendor/bootstrap-4.1.1-dist/css/bootstrap.min.css" >
	<link rel="stylesheet" href="vendor\fontawesome-free-5\css\all.min.css">
	<link rel="stylesheet" href="css/style.css">
</head>
<body>	
    <!-- Top header -->
	<div id="top-header" class="top-header bg-danger">
		<div class="container">
			<div class="row">
				<div class="col-12">
					<div class="d-flex align-items-center">
						<div class="p-2 mr-auto">
							<img class="img-fluid" src="img/logo.png" alt="logo website">
						</div>
						<div class="d-flex p-2 align-items-center">
							<div class="login mr-md-2 mr-lg-5">
								<a class="mr-3"	href="#">Đăng nhập</a>
								<a href="#">Đăng ký</a>
							</div>							
							<div class="search-form">
								<form action="#" method="post">
									<input type="search" name="search" class="form-control" placeholder="Search">
									<button type="submit"><i class="fa fa-search" aria-hidden="true"></i></button>
								</form>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- Menu -->
	<div class="menu bg-light">
		<div class="container">			
			<nav class="navbar navbar-expand-md navbar-light">			  
				<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
					<span class="navbar-toggler-icon"></span>
				</button>
				<div class="collapse navbar-collapse" id="navbarNavDropdown">
					<ul class="navbar-nav w-100 justify-content-around">
						<li class="nav-item active">
							<a class="nav-link" href="#">Trang chủ <span class="sr-only">(current)</span></a>
						</li>
						<li class="nav-item">
							<a class="nav-link" href="#">Giới thiệu</a>
						</li>			     
						<li class="nav-item dropdown">
							<a class="nav-link dropdown-toggle" href="#" id="navbarDropdownHoatDong" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
								Hoạt động
							</a>
							<div class="dropdown-menu" aria-labelledby="navbarDropdownHoatDong">
								<a class="dropdown-item" href="#">Chiến dịch thiện nguyện hè</a>
								<a class="dropdown-item" href="#">Công tác giáo dục</a>			          
							</div>
						</li>
						<li class="nav-item">
							<a class="nav-link" href="#">Liên hệ</a>
						</li>
					</ul>
				</div>
			</nav>
		</div>
	</div>

	<div class="container mt-4">
		<!-- Slideshow + tin nóng -->
		<div class="row">
			<div class="col-md-9">
				<div id="slideshow" class="carousel slide" data-ride="carousel">
					<!-- Indicators -->
					<ul class="carousel-indicators">
						<li data-target="#slideshow" data-slide-to="0" class="active"></li>
						<li data-target="#slideshow" data-slide-to="1"></li>
						<li data-target="#slideshow" data-slide-to="2"></li>
						<li data-target="#slideshow" data-slide-to="3"></li>
					</ul>				  
					<!-- The slideshow -->
					<div class="carousel-inner">
						<div class="carousel-item active">
							<img src="img/slide0.jpg" alt="Slide 0">
						</div>
						<div class="carousel-item">
							<img src="img/slide1.jpg" alt="Slide 1">
						</div>
						<div class="carousel-item">
							<img src="img/slide2.jpg" alt="Slide 2">
						</div>
						<div class="carousel-item">
							<img src="img/slide3.jpg" alt="Slide 3">
						</div>
					</div>				  
					<!-- Left and right controls -->
					<a class="carousel-control-prev" href="#slideshow" data-slide="prev">
						<span class="carousel-control-prev-icon"></span>
					</a>
					<a class="carousel-control-next" href="#slideshow" data-slide="next">
						<span class="carousel-control-next-icon"></span>
					</a>
				</div>
			</div>
			<div class="col-md-3 tinnong">
				<h4 class="bg-danger text-white text-center p-1">Tin nóng</h4>
				<ul>
					<li><a href="#">Sau chuyến thiện nguyện: cho đi và nhận lại rất nhiều</a></li>
					<li><a href="#">Người trẻ tình nguyện ra chăm chút từng góc đảo Hòn Cau</a></li>
					<li><a href="#">Tình nguyện viên quốc tế học tiếng Việt, trồng cây xanh ở Việt Nam</a></li>
					<li><a href="#">20 mùa hè xanh, bê tông hóa hàng trăm km đường xứ dừa Bến Tre</a></li>
					<li><a href="#">Mùa yêu thương trên đất Lào</a></li>
				</ul>
			</div>
		</div>	
		<!-- Bài viết mới nhất	 -->
		<div class="row mt-4 mb-3">
			<div class="col-12">
				<div class="title-section">
					<h4 class="bg-danger p-3 text-white">Bài viết mới nhất</h4>
				</div>
			</div>			
		</div>
		<div class="row">
			<div class="col-md-7">
				<div class="card w-100">
					<img class="card-img-top" src="img/tin1.jpg" alt="Để trở thành sinh viên năng động cần như thế nào?">
					<div class="card-body">
						<h4 class="card-title">Để trở thành sinh viên năng động cần như thế nào?</h4>
						<p class="card-time">10/10/2019 10:10</p>
						<p class="card-text line-clamp-3">Bạn cần nắm bắt đúng thời cơ để trở thành những sinh viên năng động. Một người trẻ năng động sẽ có nhiều cơ hội để học hỏi thêm những điều mới mẻ và có ích cho bản thân. Hiện nay tại các trường Đại học – Cao đẳng đều có các hoạt động ngoại khóa vì thế sinh viên nên tận dụng cơ hội này để có thể chọn cho mình một hoạt động như: thể thao, câu lạc bộ học thuật…</p>						
					</div>
				</div>
			</div>
			<div class="col-md-5">
				<div class="row">
					<div class="col-md-6 mb-2">
						<div class="card w-100">
							<img class="card-img-top" src="img/tin1.jpg" alt="Để trở thành sinh viên năng động cần như thế nào?">
							<div class="card-body pb-1">
								<h6 class="card-title line-clamp-2">Để trở thành sinh viên năng động cần như thế nào?</h6>
								<p class="card-time">10/10/2019 10:10</p>
							</div>
						</div>
					</div>
					<div class="col-md-6 mb-2">
						<div class="card w-100">
							<img class="card-img-top" src="img/tin1.jpg" alt="Để trở thành sinh viên năng động cần như thế nào?">
							<div class="card-body pb-1">
								<h6 class="card-title line-clamp-2">Để trở thành sinh viên năng động cần như thế nào?</h6>
								<p class="card-time">10/10/2019 10:10</p>
							</div>
						</div>
					</div>
					<div class="col-md-6 mb-2">
						<div class="card w-100">
							<img class="card-img-top" src="img/tin1.jpg" alt="Để trở thành sinh viên năng động cần như thế nào?">
							<div class="card-body pb-1">
								<h6 class="card-title line-clamp-2">Để trở thành sinh viên năng động cần như thế nào?</h6>
								<p class="card-time">10/10/2019 10:10</p>
							</div>
						</div>
					</div>
					<div class="col-md-6 mb-2">
						<div class="card w-100">
							<img class="card-img-top" src="img/tin1.jpg" alt="Để trở thành sinh viên năng động cần như thế nào?">
							<div class="card-body pb-1">
								<h6 class="card-title line-clamp-2">Để trở thành sinh viên năng động cần như thế nào?</h6>
								<p class="card-time">10/10/2019 10:10</p>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<!-- Chiến dịch tình nguyện hè + nổi bật -->
		<div class="row mt-4">
			<div class="col-md-8">
				<div class="row mb-3">
					<div class="col-12">
						<div class="title-section">
							<h4 class="bg-danger p-3 text-white">Chiến dịch tình nguyện hè</h4>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-md-6 mb-2">
						<div class="card w-100">
							<img class="card-img-top" src="img/tin1.jpg" alt="Để trở thành sinh viên năng động cần như thế nào?">
							<div class="card-body">
								<h4 class="card-title">Để trở thành sinh viên năng động cần như thế nào?</h4>
								<p class="card-time">10/10/2019 10:10</p>
								<p class="card-text line-clamp-2">Bạn cần nắm bắt đúng thời cơ để trở thành những sinh viên năng động. Một người trẻ năng động sẽ có nhiều cơ hội để học hỏi thêm những điều mới mẻ và có ích cho bản thân. Hiện nay tại các trường Đại học – Cao đẳng đều có các hoạt động ngoại khóa vì thế sinh viên nên tận dụng cơ hội này để có thể chọn cho mình một hoạt động như: thể thao, câu lạc bộ học thuật…</p>						
							</div>
						</div>
					</div>
					<div class="col-md-6 mb-2">
						<div class="card w-100">
							<img class="card-img-top" src="img/tin1.jpg" alt="Để trở thành sinh viên năng động cần như thế nào?">
							<div class="card-body">
								<h4 class="card-title">Để trở thành sinh viên năng động cần như thế nào?</h4>
								<p class="card-time">10/10/2019 10:10</p>
								<p class="card-text line-clamp-2">Bạn cần nắm bắt đúng thời cơ để trở thành những sinh viên năng động. Một người trẻ năng động sẽ có nhiều cơ hội để học hỏi thêm những điều mới mẻ và có ích cho bản thân. Hiện nay tại các trường Đại học – Cao đẳng đều có các hoạt động ngoại khóa vì thế sinh viên nên tận dụng cơ hội này để có thể chọn cho mình một hoạt động như: thể thao, câu lạc bộ học thuật…</p>						
							</div>
						</div>
					</div>
					<div class="col-md-6 mb-2">
						<div class="card w-100">
							<img class="card-img-top" src="img/tin1.jpg" alt="Để trở thành sinh viên năng động cần như thế nào?">
							<div class="card-body">
								<h4 class="card-title">Để trở thành sinh viên năng động cần như thế nào?</h4>
								<p class="card-time">10/10/2019 10:10</p>
								<p class="card-text line-clamp-2">Bạn cần nắm bắt đúng thời cơ để trở thành những sinh viên năng động. Một người trẻ năng động sẽ có nhiều cơ hội để học hỏi thêm những điều mới mẻ và có ích cho bản thân. Hiện nay tại các trường Đại học – Cao đẳng đều có các hoạt động ngoại khóa vì thế sinh viên nên tận dụng cơ hội này để có thể chọn cho mình một hoạt động như: thể thao, câu lạc bộ học thuật…</p>						
							</div>
						</div>
					</div>
					<div class="col-md-6 mb-2">
						<div class="card w-100">
							<img class="card-img-top" src="img/tin1.jpg" alt="Để trở thành sinh viên năng động cần như thế nào?">
							<div class="card-body">
								<h4 class="card-title">Để trở thành sinh viên năng động cần như thế nào?</h4>
								<p class="card-time">10/10/2019 10:10</p>
								<p class="card-text line-clamp-2">Bạn cần nắm bắt đúng thời cơ để trở thành những sinh viên năng động. Một người trẻ năng động sẽ có nhiều cơ hội để học hỏi thêm những điều mới mẻ và có ích cho bản thân. Hiện nay tại các trường Đại học – Cao đẳng đều có các hoạt động ngoại khóa vì thế sinh viên nên tận dụng cơ hội này để có thể chọn cho mình một hoạt động như: thể thao, câu lạc bộ học thuật…</p>						
							</div>
						</div>
					</div>
				</div>
				
			</div>
			<div class="col-md-4">
				<div class="row mb-3">
					<div class="col-12">
						<div class="title-section">
							<h4 class="bg-danger p-3 text-white">Nổi bật</h4>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-md-12 mb-2">
						<div class="card w-100">
							<div class="row">
								<div class="col-5">
									<img class="card-img-top" src="img/tin1.jpg" alt="Để trở thành sinh viên năng động cần như thế nào?">
								</div>
								<div class="col">
									<div class="card-body p-0">
										<h6 class="card-title line-clamp-2">Để trở thành sinh viên năng động cần như thế nào?</h6>
										<p class="card-time">10/10/2019 10:10</p>			
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="col-md-12 mb-2">
						<div class="card w-100">
							<div class="row">
								<div class="col-5">
									<img class="card-img-top" src="img/tin1.jpg" alt="Để trở thành sinh viên năng động cần như thế nào?">
								</div>
								<div class="col">
									<div class="card-body p-0">
										<h6 class="card-title line-clamp-2">Để trở thành sinh viên năng động cần như thế nào?</h6>
										<p class="card-time">10/10/2019 10:10</p>			
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="col-md-12 mb-2">
						<div class="card w-100">
							<div class="row">
								<div class="col-5">
									<img class="card-img-top" src="img/tin1.jpg" alt="Để trở thành sinh viên năng động cần như thế nào?">
								</div>
								<div class="col">
									<div class="card-body p-0">
										<h6 class="card-title line-clamp-2">Để trở thành sinh viên năng động cần như thế nào?</h6>
										<p class="card-time">10/10/2019 10:10</p>			
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="col-md-12 mb-2">
						<div class="card w-100">
							<div class="row">
								<div class="col-5">
									<img class="card-img-top" src="img/tin1.jpg" alt="Để trở thành sinh viên năng động cần như thế nào?">
								</div>
								<div class="col">
									<div class="card-body p-0">
										<h6 class="card-title line-clamp-2">Để trở thành sinh viên năng động cần như thế nào?</h6>
										<p class="card-time">10/10/2019 10:10</p>			
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="col-md-12 mb-2">
						<div class="card w-100">
							<div class="row">
								<div class="col-5">
									<img class="card-img-top" src="img/tin1.jpg" alt="Để trở thành sinh viên năng động cần như thế nào?">
								</div>
								<div class="col">
									<div class="card-body p-0">
										<h6 class="card-title line-clamp-2">Để trở thành sinh viên năng động cần như thế nào?</h6>
										<p class="card-time">10/10/2019 10:10</p>			
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				
			</div>
		</div>	
		<!-- Công tác giáo dục + phong trào -->
		<div class="row mt-4">
			<div class="col-md-8">
				<div class="row mb-3">
					<div class="col-12">
						<div class="title-section">
							<h4 class="bg-danger p-3 text-white">Công tác giáo dục</h4>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-md-4 mb-2">
						<div class="card w-100">
							<img class="card-img-top" src="img/tin1.jpg" alt="Để trở thành sinh viên năng động cần như thế nào?">
							<div class="card-body">
								<h5 class="card-title line-clamp-2">Để trở thành sinh viên năng động cần như thế nào?</h5>
								<p class="card-time">10/10/2019 10:10</p>
								<p class="card-text line-clamp-3">Bạn cần nắm bắt đúng thời cơ để trở thành những sinh viên năng động. Một người trẻ năng động sẽ có nhiều cơ hội để học hỏi thêm những điều mới mẻ và có ích cho bản thân. Hiện nay tại các trường Đại học – Cao đẳng đều có các hoạt động ngoại khóa vì thế sinh viên nên tận dụng cơ hội này để có thể chọn cho mình một hoạt động như: thể thao, câu lạc bộ học thuật…</p>						
							</div>
						</div>
					</div>
					<div class="col-md-4 mb-2">
						<div class="card w-100">
							<img class="card-img-top" src="img/tin1.jpg" alt="Để trở thành sinh viên năng động cần như thế nào?">
							<div class="card-body">
								<h5 class="card-title line-clamp-2">Để trở thành sinh viên năng động cần như thế nào?</h5>
								<p class="card-time">10/10/2019 10:10</p>
								<p class="card-text line-clamp-3">Bạn cần nắm bắt đúng thời cơ để trở thành những sinh viên năng động. Một người trẻ năng động sẽ có nhiều cơ hội để học hỏi thêm những điều mới mẻ và có ích cho bản thân. Hiện nay tại các trường Đại học – Cao đẳng đều có các hoạt động ngoại khóa vì thế sinh viên nên tận dụng cơ hội này để có thể chọn cho mình một hoạt động như: thể thao, câu lạc bộ học thuật…</p>						
							</div>
						</div>
					</div>
					<div class="col-md-4 mb-2">
						<div class="card w-100">
							<img class="card-img-top" src="img/tin1.jpg" alt="Để trở thành sinh viên năng động cần như thế nào?">
							<div class="card-body">
								<h5 class="card-title line-clamp-2">Để trở thành sinh viên năng động cần như thế nào?</h5>
								<p class="card-time">10/10/2019 10:10</p>
								<p class="card-text line-clamp-3">Bạn cần nắm bắt đúng thời cơ để trở thành những sinh viên năng động. Một người trẻ năng động sẽ có nhiều cơ hội để học hỏi thêm những điều mới mẻ và có ích cho bản thân. Hiện nay tại các trường Đại học – Cao đẳng đều có các hoạt động ngoại khóa vì thế sinh viên nên tận dụng cơ hội này để có thể chọn cho mình một hoạt động như: thể thao, câu lạc bộ học thuật…</p>						
							</div>
						</div>
					</div>
					<div class="col-md-4 mb-2">
						<div class="card w-100">
							<img class="card-img-top" src="img/tin1.jpg" alt="Để trở thành sinh viên năng động cần như thế nào?">
							<div class="card-body">
								<h5 class="card-title line-clamp-2">Để trở thành sinh viên năng động cần như thế nào?</h5>
								<p class="card-time">10/10/2019 10:10</p>
								<p class="card-text line-clamp-3">Bạn cần nắm bắt đúng thời cơ để trở thành những sinh viên năng động. Một người trẻ năng động sẽ có nhiều cơ hội để học hỏi thêm những điều mới mẻ và có ích cho bản thân. Hiện nay tại các trường Đại học – Cao đẳng đều có các hoạt động ngoại khóa vì thế sinh viên nên tận dụng cơ hội này để có thể chọn cho mình một hoạt động như: thể thao, câu lạc bộ học thuật…</p>						
							</div>
						</div>
					</div>
					<div class="col-md-4 mb-2">
						<div class="card w-100">
							<img class="card-img-top" src="img/tin1.jpg" alt="Để trở thành sinh viên năng động cần như thế nào?">
							<div class="card-body">
								<h5 class="card-title line-clamp-2">Để trở thành sinh viên năng động cần như thế nào?</h5>
								<p class="card-time">10/10/2019 10:10</p>
								<p class="card-text line-clamp-3">Bạn cần nắm bắt đúng thời cơ để trở thành những sinh viên năng động. Một người trẻ năng động sẽ có nhiều cơ hội để học hỏi thêm những điều mới mẻ và có ích cho bản thân. Hiện nay tại các trường Đại học – Cao đẳng đều có các hoạt động ngoại khóa vì thế sinh viên nên tận dụng cơ hội này để có thể chọn cho mình một hoạt động như: thể thao, câu lạc bộ học thuật…</p>						
							</div>
						</div>
					</div>
					<div class="col-md-4 mb-2">
						<div class="card w-100">
							<img class="card-img-top" src="img/tin1.jpg" alt="Để trở thành sinh viên năng động cần như thế nào?">
							<div class="card-body">
								<h5 class="card-title line-clamp-2">Để trở thành sinh viên năng động cần như thế nào?</h5>
								<p class="card-time">10/10/2019 10:10</p>
								<p class="card-text line-clamp-3">Bạn cần nắm bắt đúng thời cơ để trở thành những sinh viên năng động. Một người trẻ năng động sẽ có nhiều cơ hội để học hỏi thêm những điều mới mẻ và có ích cho bản thân. Hiện nay tại các trường Đại học – Cao đẳng đều có các hoạt động ngoại khóa vì thế sinh viên nên tận dụng cơ hội này để có thể chọn cho mình một hoạt động như: thể thao, câu lạc bộ học thuật…</p>						
							</div>
						</div>
					</div>


				</div>
				
			</div>
			<div class="col-md-4">
				<div class="row mb-3">
					<div class="col-12">
						<div class="title-section">
							<h4 class="bg-danger p-3 text-white">Phong trào</h4>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-md-12 mb-2">
						<div class="card w-100">
							<div class="row">
								<div class="col-5">
									<img class="card-img-top" src="img/tin1.jpg" alt="Để trở thành sinh viên năng động cần như thế nào?">
								</div>
								<div class="col">
									<div class="card-body p-0">
										<h6 class="card-title line-clamp-2">Để trở thành sinh viên năng động cần như thế nào?</h6>
										<p class="card-time">10/10/2019 10:10</p>			
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="col-md-12 mb-2">
						<div class="card w-100">
							<div class="row">
								<div class="col-5">
									<img class="card-img-top" src="img/tin1.jpg" alt="Để trở thành sinh viên năng động cần như thế nào?">
								</div>
								<div class="col">
									<div class="card-body p-0">
										<h6 class="card-title line-clamp-2">Để trở thành sinh viên năng động cần như thế nào?</h6>
										<p class="card-time">10/10/2019 10:10</p>			
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="col-md-12 mb-2">
						<div class="card w-100">
							<div class="row">
								<div class="col-5">
									<img class="card-img-top" src="img/tin1.jpg" alt="Để trở thành sinh viên năng động cần như thế nào?">
								</div>
								<div class="col">
									<div class="card-body p-0">
										<h6 class="card-title line-clamp-2">Để trở thành sinh viên năng động cần như thế nào?</h6>
										<p class="card-time">10/10/2019 10:10</p>			
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="col-md-12 mb-2">
						<div class="card w-100">
							<div class="row">
								<div class="col-5">
									<img class="card-img-top" src="img/tin1.jpg" alt="Để trở thành sinh viên năng động cần như thế nào?">
								</div>
								<div class="col">
									<div class="card-body p-0">
										<h6 class="card-title line-clamp-2">Để trở thành sinh viên năng động cần như thế nào?</h6>
										<p class="card-time">10/10/2019 10:10</p>			
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="col-md-12 mb-2">
						<div class="card w-100">
							<div class="row">
								<div class="col-5">
									<img class="card-img-top" src="img/tin1.jpg" alt="Để trở thành sinh viên năng động cần như thế nào?">
								</div>
								<div class="col">
									<div class="card-body p-0">
										<h6 class="card-title line-clamp-2">Để trở thành sinh viên năng động cần như thế nào?</h6>
										<p class="card-time">10/10/2019 10:10</p>			
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				
			</div>
		</div>
		<!-- Tiêu đề Hình ảnh hoạt động	 -->
		<div class="row mt-4 mb-3">
			<div class="col-12">
				<div class="title-section">
					<h4 class="bg-danger p-3 text-white">Hình ảnh hoạt động</h4>
				</div>
			</div>			
		</div>
		<!-- Slideshow hình ảnh-->
		<div class="row">
			<div class="col-md-12">
				<div id="slideshowHinhAnh" class="carousel slide" data-ride="carousel">								  
					<!-- The slideshow -->
					<div class="carousel-inner">
						<div class="carousel-item active">
							<div class="row">
								<div class="col">
									<img src="img/hinh0.jpg" alt="Hình 0">
								</div>
								<div class="col">
									<img src="img/hinh1.jpg" alt="Hình 1">
								</div>
								<div class="col">
									<img src="img/hinh2.jpg" alt="Hình 2">
								</div>
							</div>
						</div>
						<div class="carousel-item">
							<div class="row">
								<div class="col">
									<img src="img/hinh3.jpg" alt="Hình 3">
								</div>
								<div class="col">
									<img src="img/hinh4.jpg" alt="Hình 4">
								</div>
								<div class="col">
									<img src="img/hinh5.jpg" alt="Hình 5">
								</div>
							</div>
						</div>
					</div>

				</div>				  
				<!-- Left and right controls -->
				<a class="carousel-control-prev" href="#slideshowHinhAnh" data-slide="prev">
					<span class="carousel-control-prev-icon"></span>
				</a>
				<a class="carousel-control-next" href="#slideshowHinhAnh" data-slide="next">
					<span class="carousel-control-next-icon"></span>
				</a>
			</div>
		</div>
		<!-- Tiêu đề Video hoạt động -->
		<div class="row mt-4 mb-3">
			<div class="col-12">
				<div class="title-section">
					<h4 class="bg-danger p-3 text-white">Video hoạt động</h4>
				</div>
			</div>			
		</div>
		<!-- Slideshow video-->
		<div class="row">
			<div class="col-md-12">
				<div id="slideshowVideo" class="carousel slide" data-ride="carousel">								  
					<!-- The slideshow -->
					<div class="carousel-inner">
						<div class="carousel-item active">
							<div class="row">
								<div class="col-4">
									<video controls class="w-100">
										<source src="video/video1.mp4" type="video/mp4">
											Your browser does not support the video tag.
										</video>
									</div>
									<div class="col-4">
										<video controls class="w-100">
											<source src="video/mv.mp4" type="video/mp4">
												Your browser does not support the video tag.
											</video>
										</div>
										<div class="col-4">
											<video controls class="w-100">
												<source src="video/video1.mp4" type="video/mp4">
													Your browser does not support the video tag.
												</video>
											</div>
										</div>
									</div>
									<div class="carousel-item">
										<div class="row">
											<div class="col">
												<video controls class="w-100">
													<source src="video/mv.mp4" type="video/mp4">
														Your browser does not support the video tag.
													</video>
												</div>
												<div class="col">
													<video controls class="w-100">
														<source src="video/video1.mp4" type="video/mp4">
															Your browser does not support the video tag.
														</video>
													</div>
													<div class="col">
														<video controls class="w-100">
															<source src="video/mv.mp4" type="video/mp4">
																Your browser does not support the video tag.
															</video>
														</div>
													</div>
												</div>
											</div>

										</div>				  
										<!-- Left and right controls -->
										<a class="carousel-control-prev" href="#slideshowVideo" data-slide="prev">
											<span class="carousel-control-prev-icon"></span>
										</a>
										<a class="carousel-control-next" href="#slideshowVideo" data-slide="next">
											<span class="carousel-control-next-icon"></span>
										</a>
									</div>
								</div>
							</div>
						</div>
						<!-- Footer -->
						<div class="footer mt-3 text-light">
							<div class="container-fluid">
								<div class="container">
									<div class="row pt-3 pb-3">
										<!-- Logo + liên hệ -->
										<div class="col-lg-3">
											<div class="p-2 mr-auto">
												<img class="img-fluid" src="img/logo.png" alt="logo website">
												<div class="mt-3">
													<strong>Liên hệ:</strong>
													<br>
													<i class="fas fa-map-marker-alt text-danger"></i> 65 Huỳnh Thúc Kháng, P.Bến Nghé, Q.1, TP.HCM				
												</div>
											</div>
										</div>
										<!-- Danh mục 1 -->
										<div class="col-lg col-4">
											<h5>Danh mục 1</h5>
											<ul>
												<li>Danh mục 1.1</li>
												<li>Danh mục 1.2</li>
												<li>Danh mục 1.3</li>
												<li>Danh mục 1.4</li>
												<li>Danh mục 1.5</li>
											</ul>										
										</div>
										<!-- Danh mục 2 -->
										<div class="col-lg col-4">
											<h5>Danh mục 2</h5>
											<ul>
												<li>Danh mục 2.1</li>
												<li>Danh mục 2.2</li>
												<li>Danh mục 2.3</li>
												<li>Danh mục 2.4</li>
												<li>Danh mục 2.5</li>
											</ul>							
										</div>
										<!-- Danh mục 3 -->
										<div class="col-lg col-4">
											<h5>Danh mục 3</h5>
											<ul>
												<li>Danh mục 3.1</li>
												<li>Danh mục 3.2</li>
												<li>Danh mục 3.3</li>
												<li>Danh mục 3.4</li>
												<li>Danh mục 3.5</li>
											</ul>
										</div>
										<!-- Đóng góp -->
										<div class="col-lg-3 text-right">					
											<textarea class="form-control" rows="4" id="comment" placeholder="Mời bạn đóng góp ý kiến"></textarea>
											<button type="button" class="btn btn-warning mt-2 w-100">Gửi</button>
										</div>
									</div>
								</div>
							</div>
						</div>
						<!-- Nút quay về đầu trang -->
						<div class="btn-go-up">							
							<a href="#top-header" class="fas fa-arrow-circle-up"></a>
						</div>

						<script src="vendor/js/jquery-3.3.1.min.js"></script>
						<script src="vendor/js/popper.min.js"></script>
						<script src="vendor/bootstrap-4.1.1-dist/js/bootstrap.min.js" ></script>
						<script src="js/script.js"></script>
					</body>	
				</body>
				</html>

