<html>
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<title>Hoạt động thiện nguyện sinh viên</title>
	<link rel="stylesheet" href="vendor/bootstrap-4.1.1-dist/css/bootstrap.min.css">
	<link rel="stylesheet" href="vendor\fontawesome-free-5\css\all.min.css">
	<link rel="stylesheet" href="css/style.css">
</head>
<body>
	<!-- Top header -->
	<div id="top-header" class="top-header bg-danger">
		<div class="container">
			<div class="row">
				<div class="col-12">
					<div class="d-flex align-items-center">
						<div class="p-2 mr-auto">
							<img class="img-fluid" src="img/logo.png" alt="logo website">
						</div>
						<div class="d-flex p-2 align-items-center">
							<div class="login mr-md-2 mr-lg-5">
								<a class="mr-3" href="#">Đăng nhập</a> <a href="#">Đăng ký</a>
							</div>
							<div class="search-form">
								<form action="#" method="post">
									<input type="search" name="search" class="form-control" placeholder="Search">
									<button type="submit">
										<i class="fa fa-search" aria-hidden="true"></i>
									</button>
								</form>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- Menu -->
	<div class="menu bg-light">
		<div class="container">
			<nav class="navbar navbar-expand-md navbar-light">			  
				<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
					<span class="navbar-toggler-icon"></span>
				</button>
				<div class="collapse navbar-collapse" id="navbarNavDropdown">
					<ul class="navbar-nav w-100 justify-content-around">
						<li class="nav-item active">
							<a class="nav-link" href="#">Trang chủ <span class="sr-only">(current)</span></a>
						</li>
						<li class="nav-item">
							<a class="nav-link" href="#">Giới thiệu</a>
						</li>			     
						<li class="nav-item dropdown">
							<a class="nav-link dropdown-toggle" href="#" id="navbarDropdownHoatDong" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
								Hoạt động
							</a>
							<div class="dropdown-menu" aria-labelledby="navbarDropdownHoatDong">
								<a class="dropdown-item" href="#">Chiến dịch thiện nguyện hè</a>
								<a class="dropdown-item" href="#">Công tác giáo dục</a>			          
							</div>
						</li>
						<li class="nav-item">
							<a class="nav-link" href="#">Liên hệ</a>
						</li>
					</ul>
				</div>
			</nav>
		</div>
	</div>
	<div class="container mt-3">
        <!-- Điều hướng -->
		<div class="row">
			<div class="col-12">
				<ul class="breadcrumb">
					<li><a href="#">Hoạt động</a></li>
					<li><a href="#">Chiến dịch tình nguyện hè</a></li>
				</ul>
			</div>
			<div class="col-12">
				<h4 class="font-weight-light text-search">Kết quả tìm kiếm cho 'thanh niên'</h4>
			</div>
		</div>
		<!-- Danh sách sản phẩm -->
		<div class="row">
			<div class="col-lg-3 col-md-6 mb-3">
				<div class="card w-100">
					<img class="card-img-top" src="img/tin1.jpg" alt="Để trở thành sinh viên năng động cần như thế nào?">
					<div class="card-body">
						<h4 class="card-title">Để trở thành sinh viên năng động cần như thế nào?</h4>
						<p class="card-time">10/10/2019 10:10</p>
						<p class="card-text line-clamp-3">Bạn cần nắm bắt đúng thời cơ để trở thành những sinh viên năng động. Một người trẻ năng động sẽ có nhiều cơ hội để học hỏi thêm những điều mới mẻ và có ích cho bản thân. Hiện nay tại các trường Đại học – Cao đẳng đều có các hoạt động ngoại khóa vì thế sinh viên nên tận dụng cơ hội này để có thể chọn cho mình một hoạt động như: thể thao, câu lạc bộ học thuật…</p>						
					</div>
				</div>
			</div>
			<div class="col-lg-3 col-md-6 mb-3">
				<div class="card w-100">
					<img class="card-img-top" src="img/tin1.jpg" alt="Để trở thành sinh viên năng động cần như thế nào?">
					<div class="card-body">
						<h4 class="card-title">Để trở thành sinh viên năng động cần như thế nào?</h4>
						<p class="card-time">10/10/2019 10:10</p>
						<p class="card-text line-clamp-3">Bạn cần nắm bắt đúng thời cơ để trở thành những sinh viên năng động. Một người trẻ năng động sẽ có nhiều cơ hội để học hỏi thêm những điều mới mẻ và có ích cho bản thân. Hiện nay tại các trường Đại học – Cao đẳng đều có các hoạt động ngoại khóa vì thế sinh viên nên tận dụng cơ hội này để có thể chọn cho mình một hoạt động như: thể thao, câu lạc bộ học thuật…</p>						
					</div>
				</div>
			</div>
			<div class="col-lg-3 col-md-6 mb-3">
				<div class="card w-100">
					<img class="card-img-top" src="img/tin1.jpg" alt="Để trở thành sinh viên năng động cần như thế nào?">
					<div class="card-body">
						<h4 class="card-title">Để trở thành sinh viên năng động cần như thế nào?</h4>
						<p class="card-time">10/10/2019 10:10</p>
						<p class="card-text line-clamp-3">Bạn cần nắm bắt đúng thời cơ để trở thành những sinh viên năng động. Một người trẻ năng động sẽ có nhiều cơ hội để học hỏi thêm những điều mới mẻ và có ích cho bản thân. Hiện nay tại các trường Đại học – Cao đẳng đều có các hoạt động ngoại khóa vì thế sinh viên nên tận dụng cơ hội này để có thể chọn cho mình một hoạt động như: thể thao, câu lạc bộ học thuật…</p>						
					</div>
				</div>
			</div>
			<div class="col-lg-3 col-md-6 mb-3">
				<div class="card w-100">
					<img class="card-img-top" src="img/tin1.jpg" alt="Để trở thành sinh viên năng động cần như thế nào?">
					<div class="card-body">
						<h4 class="card-title">Để trở thành sinh viên năng động cần như thế nào?</h4>
						<p class="card-time">10/10/2019 10:10</p>
						<p class="card-text line-clamp-3">Bạn cần nắm bắt đúng thời cơ để trở thành những sinh viên năng động. Một người trẻ năng động sẽ có nhiều cơ hội để học hỏi thêm những điều mới mẻ và có ích cho bản thân. Hiện nay tại các trường Đại học – Cao đẳng đều có các hoạt động ngoại khóa vì thế sinh viên nên tận dụng cơ hội này để có thể chọn cho mình một hoạt động như: thể thao, câu lạc bộ học thuật…</p>						
					</div>
				</div>
			</div>
			<div class="col-lg-3 col-md-6 mb-3">
				<div class="card w-100">
					<img class="card-img-top" src="img/tin1.jpg" alt="Để trở thành sinh viên năng động cần như thế nào?">
					<div class="card-body">
						<h4 class="card-title">Để trở thành sinh viên năng động cần như thế nào?</h4>
						<p class="card-time">10/10/2019 10:10</p>
						<p class="card-text line-clamp-3">Bạn cần nắm bắt đúng thời cơ để trở thành những sinh viên năng động. Một người trẻ năng động sẽ có nhiều cơ hội để học hỏi thêm những điều mới mẻ và có ích cho bản thân. Hiện nay tại các trường Đại học – Cao đẳng đều có các hoạt động ngoại khóa vì thế sinh viên nên tận dụng cơ hội này để có thể chọn cho mình một hoạt động như: thể thao, câu lạc bộ học thuật…</p>						
					</div>
				</div>
			</div>
			<div class="col-lg-3 col-md-6 mb-3">
				<div class="card w-100">
					<img class="card-img-top" src="img/tin1.jpg" alt="Để trở thành sinh viên năng động cần như thế nào?">
					<div class="card-body">
						<h4 class="card-title">Để trở thành sinh viên năng động cần như thế nào?</h4>
						<p class="card-time">10/10/2019 10:10</p>
						<p class="card-text line-clamp-3">Bạn cần nắm bắt đúng thời cơ để trở thành những sinh viên năng động. Một người trẻ năng động sẽ có nhiều cơ hội để học hỏi thêm những điều mới mẻ và có ích cho bản thân. Hiện nay tại các trường Đại học – Cao đẳng đều có các hoạt động ngoại khóa vì thế sinh viên nên tận dụng cơ hội này để có thể chọn cho mình một hoạt động như: thể thao, câu lạc bộ học thuật…</p>						
					</div>
				</div>
			</div>
			<div class="col-lg-3 col-md-6 mb-3">
				<div class="card w-100">
					<img class="card-img-top" src="img/tin1.jpg" alt="Để trở thành sinh viên năng động cần như thế nào?">
					<div class="card-body">
						<h4 class="card-title">Để trở thành sinh viên năng động cần như thế nào?</h4>
						<p class="card-time">10/10/2019 10:10</p>
						<p class="card-text line-clamp-3">Bạn cần nắm bắt đúng thời cơ để trở thành những sinh viên năng động. Một người trẻ năng động sẽ có nhiều cơ hội để học hỏi thêm những điều mới mẻ và có ích cho bản thân. Hiện nay tại các trường Đại học – Cao đẳng đều có các hoạt động ngoại khóa vì thế sinh viên nên tận dụng cơ hội này để có thể chọn cho mình một hoạt động như: thể thao, câu lạc bộ học thuật…</p>						
					</div>
				</div>
			</div>
			<div class="col-lg-3 col-md-6 mb-3">
				<div class="card w-100">
					<img class="card-img-top" src="img/tin1.jpg" alt="Để trở thành sinh viên năng động cần như thế nào?">
					<div class="card-body">
						<h4 class="card-title">Để trở thành sinh viên năng động cần như thế nào?</h4>
						<p class="card-time">10/10/2019 10:10</p>
						<p class="card-text line-clamp-3">Bạn cần nắm bắt đúng thời cơ để trở thành những sinh viên năng động. Một người trẻ năng động sẽ có nhiều cơ hội để học hỏi thêm những điều mới mẻ và có ích cho bản thân. Hiện nay tại các trường Đại học – Cao đẳng đều có các hoạt động ngoại khóa vì thế sinh viên nên tận dụng cơ hội này để có thể chọn cho mình một hoạt động như: thể thao, câu lạc bộ học thuật…</p>						
					</div>
				</div>
			</div>

		</div>
		<!-- Phân trang -->
		<div class="row mt-4">
			<ul class="pagination justify-content-center w-100">
				<li class="page-item"><a class="page-link" href="javascript:void(0);">Trang trước</a></li>
				<li class="page-item active"><a class="page-link" href="javascript:void(0);">1</a></li>
				<li class="page-item"><a class="page-link" href="javascript:void(0);">2</a></li>
				<li class="page-item"><a class="page-link" href="javascript:void(0);">Trang sau</a></li>
			</ul>
		</div>
	</div>
</div>
<!-- Footer -->
<div class="footer mt-3 text-light">
	<div class="container-fluid">
		<div class="container">
			<div class="row pt-3 pb-3">
				<!-- Logo + liên hệ -->
				<div class="col-lg-3">
					<div class="p-2 mr-auto">
						<img class="img-fluid" src="img/logo.png" alt="logo website">
						<div class="mt-3">
							<strong>Liên hệ:</strong>
							<br>
							<i class="fas fa-map-marker-alt text-danger"></i>65 Huỳnh Thúc Kháng, P.Bến Nghé,
							Q.1, TP.HCM
						</div>
					</div>
				</div>
				<!-- Danh mục 1 -->
				<div class="col-lg col-4">
					<h5>
					Danh mục 1</h5>
					<ul>
						<li>Danh mục 1.1</li>
						<li>Danh mục 1.2</li>
						<li>Danh mục 1.3</li>
						<li>Danh mục 1.4</li>
						<li>Danh mục 1.5</li>
					</ul>
				</div>
				<!-- Danh mục 2 -->
				<div class="col-lg col-4">
					<h5>
					Danh mục 2</h5>
					<ul>
						<li>Danh mục 2.1</li>
						<li>Danh mục 2.2</li>
						<li>Danh mục 2.3</li>
						<li>Danh mục 2.4</li>
						<li>Danh mục 2.5</li>
					</ul>
				</div>
				<!-- Danh mục 3 -->
				<div class="col-lg col-4">
					<h5>
					Danh mục 3</h5>
					<ul>
						<li>Danh mục 3.1</li>
						<li>Danh mục 3.2</li>
						<li>Danh mục 3.3</li>
						<li>Danh mục 3.4</li>
						<li>Danh mục 3.5</li>
					</ul>
				</div>
				<!-- Đóng góp -->
				<div class="col-lg-3 text-right">
					<textarea class="form-control" rows="4" id="comment" placeholder="Mời bạn đóng góp ý kiến"></textarea>
					<button type="button" class="btn btn-warning mt-2 w-100">
					Gửi</button>
				</div>
			</div>
		</div>
	</div>
</div>
<!-- Nút quay về đầu trang -->
<div class="btn-go-up">
	<a href="#top-header" class="fas fa-arrow-circle-up"></a>
</div>
<script src="vendor/js/jquery-3.3.1.min.js"></script>
<script src="vendor/js/popper.min.js"></script>
<script src="vendor/bootstrap-4.1.1-dist/js/bootstrap.min.js"></script>
<script src="js/script.js"></script>
</body>
</body>
</html>
